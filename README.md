# pulser-test-summary-plots

## What is this?
- This is for the ATLAS New Small Wheel, to plot the data from the pulser test outlined [here](https://twiki.cern.ch/twiki/bin/view/Atlas/STGCPulserTestCERN)
- The pulser test inputs a square wave into each segment (strip, pad, or wire) of an sTGC quad, and measures the segment's response
- This code makes plots of pulser test results, amplitude (of the segment's response) vs. strip/pad/wire number (Alam's mapping, but condatenated for each quad, e.g. if QS1 pads stop at 70, QS2 pads will start at 71)

## Before you run anything
- Plots are saved in the ``plots'' directory, you may not need to actually run this yourself

## How to run the code
- Clone this repository (``git clone https://gitlab.cern.ch/camccrac/pulser-test-summary-plots.git'')
- Enter repo directory ``cd pulser-test-summary-plots''
- Install dependencies (python 3, numpy, matplotlib)
  - This can be done through [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) using ``conda env create -f environment.yml --name summary_plot_env && conda activate summary_plot_env''
- run python files (I assume python = python 3, I used 3.8)
  - ``python Summary_full_wedge.py'': saves plots for each individual wedge
  - ``python all_wedge_plot.py'': makes an interactive plot showing each wedge's data in context
