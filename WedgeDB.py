import re


class Wedge():
    """
    Class for containing wedge data.

    Properties set on initialization:
    number: int, 1 to 70
    quads: list, e.g. ['QS1.P.2', 'QS2.P.1', 'QS3.P.2'] (123 order matters)
    alias: str, e.g. 'C-SC1' or 'LP12'
    sectorName: str, e.g. 'CO2-HO' or 'A14-IP'
    """
    def __init__(self, number: int, quads: list, alias: str, sectorName: str):
        # check number
        if number not in range(1, 71):
            raise ValueError(f'Invalid wedge number: {number}')
        # check quads are of the right format
        if len(quads) != 3:
            raise ValueError(f"All wedges have 3 quads, not {len(quads)}")
        for quad in quads:
            if re.match(r"Q[LS][123]\.[PC]\.\d+", quad) is None:
                raise ValueError(f"Invalid quad name: {quad}")
        # check alias
        if re.match(r"(C-)?[LS][PC]\d+", alias) is None:
            raise ValueError(f"Invalid wedge alias: {alias}")
        # check sectorName
        if sectorName is None or sectorName == "DISCARDED":
            pass
        elif len(sectorName) != 6:
            raise ValueError('Sector name, must be 6 chars long, e.g. C02-HO')
        elif re.match(r"[AC]\d\d-(IP|HO)", sectorName) is None:
            raise ValueError(f'Invalid sector name: {sectorName}')
        self.number = number
        self.quads = quads
        self.alias = alias
        self.sectorName = sectorName


wedges = {
    # e.g. Wedge(x, ['QS1.P.x', 'QS2.P.x', 'QS3.P.x'], "C-SC1", "A14-IP")
    1: Wedge(1, ['QS1.P.2', 'QS2.P.1', 'QS3.P.2'], "SP1", "DISCARDED"),
    2: Wedge(2, ['QS1.P.4', 'QS2.P.7', 'QS3.P.6'], "SP2", "A08-HO"),
    3: Wedge(3, ['QS1.P.8', 'QS2.P.4', 'QS3.P.1'], "SP3", "A12-HO"),
    4: Wedge(4, ['QS1.P.10', 'QS2.P.10', 'QS3.P.8'], "SP4", "A16-HO"),
    5: Wedge(5, ['QS1.P.7', 'QS2.P.5', 'QS3.P.7'], "SP5", "A14-HO"),
    6: Wedge(6, ['QS1.P.9', 'QS2.P.6', 'QS3.P.10'], "SP6", "A10-HO"),
    7: Wedge(7, ['QS1.C.1', 'QS2.C.10', 'QS3.C.3'], "SC1", "A16-IP"),
    8: Wedge(8, ['QS1.C.2', 'QS2.C.13', 'QS3.C.2'], "SC2", "A12-IP"),
    9: Wedge(9, ['QS1.C.3', 'QS2.C.9', 'QS3.C.4'], "SC3", "A14-IP"),
    10: Wedge(10, ['QS1.C.4', 'QS2.C.15', 'QS3.C.10'], "SC4", "A10-IP"),
    11: Wedge(11, ['QL1.P.7', 'QL2.P.1', 'QL3.P.4'], "LP1", "A05-IP"),
    12: Wedge(12, ['QS1.C.6', 'QS2.C.16', 'QS3.C.11'], "SC5", "DISCARDED"),
    13: Wedge(13, ['QL1.P.9', 'QL2.P.2', 'QL3.P.2'], "LP2", "A13-IP"),
    14: Wedge(14, ['QS1.C.5', 'QS2.C.0', 'QS3.C.12'], "SC6", "A08-IP"),
    15: Wedge(15, ['QL1.P.5', 'QL2.P.3', 'QL3.P.3'], "LP3", "A15-IP"),
    16: Wedge(16, ['QS1.C.7', 'QS2.C.12', 'QS3.C.8'], "SC7", "A06-IP"),
    17: Wedge(17, ['QS1.C.8', 'QS2.C.11', 'QS3.C.7'], "SC8", "A02-IP"),
    18: Wedge(18, ['QL1.P.10', 'QL2.P.4', 'QL3.P.7'], "LP4", "A09-IP"),
    19: Wedge(19, ['QL1.P.12', 'QL2.P.5', 'QL3.P.9'], "LP5", "A11-IP"),
    20: Wedge(20, ['QS1.P.12', 'QS2.P.8', 'QS3.P.9'], "SP7", "A02-HO"),
    21: Wedge(21, ['QL1.C.2', 'QL2.C.1', 'QL3.C.6'], "LC1", "A13-HO"),
    22: Wedge(22, ['QS1.P.13', 'QS2.P.11', 'QS3.P.11'], "SP8", "A06-HO"),
    23: Wedge(23, ['QL1.C.3', 'QL2.C.4', 'QL3.C.2'], "LC2", "A11-HO"),
    24: Wedge(24, ['QL1.C.1', 'QL2.C.2', 'QL3.C.5'], "LC3", "A15-HO"),
    25: Wedge(25, ['QL1.P.6', 'QL2.P.6', 'QL3.P.8'], "LP6", "A01-IP"),
    26: Wedge(26, ['QL1.P.13', 'QL2.P.7', 'QL3.P.6'], "LP7", "A07-IP"),
    27: Wedge(27, ['QL1.C.7', 'QL2.C.3', 'QL3.C.7'], "LC4", "A09-HO"),
    28: Wedge(28, ['QL1.C.10', 'QL2.C.5', 'QL3.C.8'], "LC5", "A01-HO"),
    29: Wedge(29, ['QL1.C.8', 'QL2.C.6', 'QL3.C.3'], "LC6", "A07-HO"),
    30: Wedge(30, ['QL1.P.14', 'QL2.P.8', 'QL3.P.5'], "LP8", "A03-IP"),
    31: Wedge(31, ['QS1.P.14', 'QS2.P.13', 'QS3.P.12'], "SP9", "A04-HO"),
    32: Wedge(32, ['QL1.C.11', 'QL2.C.8', 'QL3.C.1'], "LC7", "A03-HO"),
    33: Wedge(33, ['QL1.C.9', 'QL2.C.7', 'QL3.C.11'], "LC8", "A05-HO"),
    34: Wedge(34, ['QS1.C.9', 'QS2.C.2', 'QS3.C.6'], "SC9", "A04-IP"),
    35: Wedge(35, ['QL1.C.5', 'QL2.C.9', 'QL3.C.10'], "C-LC1", "C13-HO"),
    36: Wedge(36, ['QL1.P.8', 'QL2.P.9', 'QL3.P.10'], "C-LP1", "C13-IP"),
    37: Wedge(37, ['QS1.P.15', 'QS2.P.15', 'QS3.P.3'], "C-SP1", "C12-HO"),
    38: Wedge(38, ['QL1.C.14', 'QL2.C.10', 'QL3.C.9'], "C-LC2", "C11-HO"),
    39: Wedge(39, ['QS1.P.17', 'QS2.P.16', 'QS3.P.13'], "C-SP2", "C14-HO"),
    40: Wedge(40, ['QL1.P.4', 'QL2.P.10', 'QL3.P.14'], "C-LP2", "C11-IP"),
    41: Wedge(41, ['QL1.C.17', 'QL2.C.11', 'QL3.C.17'], "C-LC3", "C15-HO"),
    42: Wedge(42, ['QL1.P.16', 'QL2.P.11', 'QL3.P.13'], "C-LP3", "C15-IP"),
    43: Wedge(43, ['QL1.C.16', 'QL2.C.12', 'QL3.C.15'], "C-LC4", "C09-HO"),
    44: Wedge(44, ['QS1.C.10', 'QS2.C.17', 'QS3.C.16'], "C-SC1", "C12-IP"),
    45: Wedge(45, ['QL1.P.18', 'QL2.P.12', 'QL3.P.12'], "C-LP4", "C09-IP"),
    46: Wedge(46, ['QS1.P.16', 'QS2.P.20', 'QS3.P.14'], "C-SP3", "C10-HO"),
    47: Wedge(47, ['QL1.C.12', 'QL2.C.13', 'QL3.C.14'], "C-LC5", "C01-HO"),
    48: Wedge(48, ['QS1.P.19', 'QS2.P.17', 'QS3.P.15'], "C-SP4", "C16-HO"),
    49: Wedge(49, ['QL1.P.15', 'QL2.P.13', 'QL3.P.15'], "C-LP5", "C03-IP"),
    50: Wedge(50, ['QS1.C.11', 'QS2.C.1', 'QS3.C.15'], "C-SC2", "C14-IP"),
    51: Wedge(51, ['QS1.P.18', 'QS2.P.12', 'QS3.P.16'], "C-SP5", "C08-HO"),
    52: Wedge(52, ['QL1.P.11', 'QL2.P.14', 'QL3.P.16'], "C-LP6", "C05-IP"),
    53: Wedge(53, ['QL1.C.6', 'QL2.C.14', 'QL3.C.13'], "C-LC6", "C07-HO"),
    54: Wedge(54, ['QS1.C.51', 'QS2.C.14', 'QS3.C.14'], "C-SC3", "C10-IP"),
    55: Wedge(55, ['QL1.C.19', 'QL2.C.15', 'QL3.C.16'], "C-LC7", None),
    56: Wedge(56, ['QL1.P.19', 'QL2.P.15', 'QL3.P.18'], "C-LP7", "C09-IP"),
    57: Wedge(57, ['QL1.C.15', 'QL2.C.17', 'QL3.C.12'], "C-LC8", "C03-HO"),
    58: Wedge(58, ['QS1.C.13', 'QS2.C.18', 'QS3.C.19'], "C-SC4", "C16-IP"),
    59: Wedge(59, ['QS1.P.20', 'QS2.P.9', 'QS3.P.17'], "C-SP6", "C02-HO"),
    60: Wedge(60, ['QL1.P.2', 'QL2.P.16', 'QL3.P.1'], "C-LP8", "C01-IP"),
    61: Wedge(61, ['QS1.C.12', 'QS2.C.3', 'QS3.C.1'], "C-SC5", "C08-IP"),
    62: Wedge(62, ['QL1.P.20', 'QL2.P.17', 'QL3.P.19'], "C-LP9", "C07-IP"),
    63: Wedge(63, ['QL1.C.18', 'QL2.C.18', 'QL3.C.19'], "C-LC9", "C05-HO"),
    64: Wedge(64, ['QS1.P.21', 'QS2.P.19', 'QS3.P.18'], "C-SP7", "C06-HO"),
    65: Wedge(65, ['QS1.P.11', 'QS2.P.18', 'QS3.C.17'], "C-SP8", None),  # Q3 C
    66: Wedge(66, ['QS1.C.53', 'QS2.C.6', 'QS3.C.13'], "C-SC6", "C04-IP"),
    67: Wedge(67, ['QS1.C.52', 'QS2.C.19', 'QS3.C.9'], "C-SC7", "C02-IP"),
    68: Wedge(68, ['QS1.C.14', 'QS2.C.5', 'QS3.C.18'], "C-SC8", "C06-IP"),
    69: Wedge(69, ['QS1.P.22', 'QS2.P.14', 'QS3.P.4'], "C-SP9", "C04-HO"),
    70: Wedge(70, ['QS1.C.54', 'QS2.C.8', 'QS3.P.19'], "C-SC9", None)  # Q3 P
}

# the same dict but with key = alias rather than wedge number
alias_dict = {}
for _, w in wedges.items():
    alias_dict[w.alias] = w
# same for sector name
sector_dict = {}
for _, w in wedges.items():
    sector_dict[w.sectorName] = w


def getWedgeByNumber(num):
    return wedges[num]


def getWedgeByAlias(alias):
    return alias_dict[alias]


def getWedgeBySectorName(sectorName):
    return sector_dict[sectorName]


if __name__ == '__main__':
    # should all print ['QS1.P.4', 'QS2.P.7', 'QS3.P.6']
    print(getWedgeByNumber(2).quads)
    print(getWedgeByAlias("SP2").quads)
    print(getWedgeBySectorName("A08-HO").quads)
