exceptions = [
    # Wedge 7
    ['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer2_P_GFZP1', 217],  # L2 ends at 216
    ['May_14_sTGC_QS2_CONFIRM-10_Layer2_P_GFZP1', 227],  # L2 ends at 226
    ['Jun_26_sTGC_QS3_CONFIRM-3_Layer4_P_GFZP1', 235.0],  # L4 ends at 234
    # 8
    ['Jun_21_sTGC_QS2_CONFIRM-13_Layer2_P_GFZP1', 227.0],  # L2 ends at 226
    # 11
    ['Sep_23_sTGC_QL2_PIVOT-1_Layer1_P_GFZP1', 33],  # L1 starts at 34
    ['Sep_23_sTGC_QL2_PIVOT-1_Layer2_P_GFZP1', 94],  # L2 ends at 93
    # 18
    ['Jan_23_sTGC_QL2_PIVOT-4_Layer2_P_GFZP1', 34.0],  # L2 starts at 38
    ['Jan_23_sTGC_QL2_PIVOT-4_Layer2_P_GFZP1', 35.0],  # L1 starts at 34 though
    ['Jan_23_sTGC_QL2_PIVOT-4_Layer2_P_GFZP1', 36.0],
    ['Jan_23_sTGC_QL2_PIVOT-4_Layer2_P_GFZP1', 37.0],
    # 60
    ['Aug_15_sTGC_QL3_PIVOT-1_Layer1_P_GFZP1', 255.0],  # L1/L4 go to 254
    ['Aug_15_sTGC_QL3_PIVOT-1_Layer4_P_GFZP1', 255.0],  # but L2/L3 go to 255
]
