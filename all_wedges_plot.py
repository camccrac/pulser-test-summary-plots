"""
Makes an interactive plot showing a wedge's pulser test results as compared to others of the same kind.
(Here, 'kind' means SP, SC, LP, or LC.)

Just run this file "python all_wedges_plot.py"
"""

import pickle
from WedgeDB import wedges
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Button


# initial plot setup stuff
fig, (strip_ax, pad_ax, wire_ax) = plt.subplots(3, 1, figsize=(10, 14))
plt.subplots_adjust(bottom=0.2)
cmap = {1: 'blue', 2: 'red', 3: 'gold', 4: 'green'}  # keys are layers

# store line objects after first plot so it's faster
lines = {}
wedge_btns, wedge_btn_axes, wedges_to_plot, strip_dicts, pad_dicts, wire_dicts = [], [], [], [], [], []
kind_names = ['SP', 'SC', 'LP', 'LC']
current_kind = None  # should be set later

def highlight_wedge(selected):
    """Return plot function for wedge buttons"""
    def plot(event):
        """updates strip, pad, wire plots by highlighting one wedge"""
        # update button colours
        global wedge_buttons, wedges_to_plot
        for i, w in enumerate(wedges_to_plot):
            btn = wedge_btns[i]
            if w.number == selected:
                btn.color = 'lightgreen'
            else:
                btn.color = 'white'
        
        # loop over strips, pads, wires
        for dicts_list, ax, pintype in zip(
                [strip_dicts, pad_dicts, wire_dicts],
                [strip_ax, pad_ax, wire_ax],
                ['strip', 'pad', 'wire']):
            # and over data for each wedge
            for d, w in zip(dicts_list, wedges_to_plot):
                tuples = np.array(list(d.keys()))
                channels = tuples[:,0]
                layers = tuples[:,1]
                amplitudes = np.array(list(d.values()))
                sort_indices = channels.argsort()
                channels = channels[sort_indices]
                layers = layers[sort_indices]
                amplitudes = amplitudes[sort_indices]
                for layer in [1,2,3,4]:
                    # set plot parameters
                    if w.number == selected:
                        color = cmap[layer]
                        lw = 2
                        alpha = 1
                        zorder = 10
                    else:
                        color = 'k'
                        lw = 0.1
                        alpha = 0.75
                        zorder = -1
                    x = channels[layers==layer]
                    y = amplitudes[layers==layer]
                    # plot, only actually using ax.plot if we haven't plotted yet                   
                    if (w.number, layer, pintype) in lines.keys():
                        lines[(w.number, layer, pintype)].set_lw(lw)
                        lines[(w.number, layer, pintype)].set_alpha(alpha)
                        lines[(w.number, layer, pintype)].set_zorder(zorder)
                        lines[(w.number, layer, pintype)].set_color(color)
                    else:
                        lines[(w.number, layer, pintype)], = ax.plot(
                            x, y, color, lw=lw, alpha=alpha, zorder=zorder)
        plt.draw()
    return plot



def select_kind(kind):
    """Returns a function that selects a new kind of wedge to plot, e.g. LP"""
    def inner(event):
        """Initializes graphs and data for plotting a new kind of wedge"""
        global wedge_btns, wedge_btn_axes, wedges_to_plot, strip_dicts, pad_dicts, wire_dicts, current_kind, lines

        # make sure this is a valid kind and not the one we're already plotting
        assert kind in kind_names
        if kind == current_kind:
            return

        # clear axes
        strip_ax.clear()
        pad_ax.clear()
        wire_ax.clear()

        # add labels and such
        for ax in [strip_ax, pad_ax, wire_ax]:
            ax.grid(which='major')
        strip_ax.set_ylabel('Strips: Amplitude (V)', fontsize=14)
        pad_ax.set_ylabel('Pads: Amplitude (V)', fontsize=14)
        wire_ax.set_ylabel('Wires: Amplitude (V)', fontsize=14)
        wire_ax.set_xlabel('Strip/Pad/Wire Number', fontsize=14)

        # clear buttons
        for ax in wedge_btn_axes:
            ax.remove()

        # reset stored line objects
        lines = {}

        wedges_to_plot = [w for _, w in wedges.items() if kind in w.alias]

        # open pickles for all wedges of type
        strip_dicts = [pickle.load(open(f"pickles/strip_data_wedge_{w.number}.p", "rb")) for w in wedges_to_plot]
        pad_dicts = [pickle.load(open(f"pickles/pad_data_wedge_{w.number}.p", "rb")) for w in wedges_to_plot]
        wire_dicts = [pickle.load(open(f"pickles/wire_data_wedge_{w.number}.p", "rb")) for w in wedges_to_plot]

        # make new wedge buttons
        wedge_names = [w.alias for w in wedges_to_plot]
        wedge_nums = [w.number for w in wedges_to_plot]
        n_wedge_buttons = len(wedges_to_plot)
        wedge_btn_y_height = 0.025
        wedge_btn_y_posn = 0.05
        wedge_axes_specs = [[1/n_wedge_buttons * i, 1/(2*n_wedge_buttons), wedge_btn_y_posn, wedge_btn_y_height] for i in range(len(wedges_to_plot))]
        wedge_btn_axes = [plt.axes(spec) for spec in wedge_axes_specs]
        wedge_btns = [Button(ax, w) for ax, w in zip(wedge_btn_axes, wedge_names)]
        for btn, num in zip(wedge_btns, wedge_nums):
            btn.on_clicked(highlight_wedge(num))
        # run plot once with first button selected
        highlight_wedge(wedge_nums[0])(None)
        # and set this so we know not to run again if same button hit twice
        current_kind = kind
    return inner
        
# make kind buttons
n_kind_buttons = len(kind_names)
kind_btn_y_height = 0.025
kind_btn_y_posn = 0.1
kind_axes_specs = [[1/n_kind_buttons * i, 1/(2*n_kind_buttons), kind_btn_y_posn, kind_btn_y_height] for i in range(len(kind_names))]
kind_btn_axes = [plt.axes(spec) for spec in kind_axes_specs]
kind_btns = [Button(ax, name) for ax, name in zip(kind_btn_axes, kind_names)]
for btn, kind in zip(kind_btns, kind_names):
    btn.on_clicked(select_kind(kind))
# run once with first button selected
select_kind(kind_names[0])(None)

# show
plt.show()



