import subprocess
import os
import sys
import errno

from QABmap import getFolderUniqueName


def getQuadUniqueIDList(wedge):
    quadUniqueIDlist = []
    for quad in ['QS1', 'QS2', 'QS3', 'QL1', 'QL2', 'QL3']:
        if getQuadUniqueID(wedge, quad) != '': 
            quadUniqueIDlist.extend([getQuadUniqueID(wedge, quad)])
    return quadUniqueIDlist


def getQuadUniqueID(wedge, quadid):
    quadUniqueID = ''
    if 'WEDGE1' in wedge:
        if 'QS3' in quadid:
            quadUniqueID = 'QS3.P.2'
        if 'QS2' in quadid:
            quadUniqueID = 'QS2.P.1'
        if 'QS1' in quadid:
            quadUniqueID = 'QS1.P.2'
    elif 'WEDGE2' in wedge:
        if 'QS3' in quadid:
            quadUniqueID = 'QS3.P.6'
        if 'QS2' in quadid:
            quadUniqueID = 'QS2.P.7'
        if 'QS1' in quadid:
            quadUniqueID = 'QS1.P.4'
    elif quadid in wedge:
        return wedge
    return quadUniqueID


def getFoldersList(wedge, quadid, layer, ab, gfz, hv):
    QuadIdList = []
    if wedge != '' and quadid == '':
        QuadIdList.extend(getQuadUniqueIDList(wedge))
    if wedge != '' and quadid != '':
        QuadIdList.extend(getQuadUniqueID(wedge, quadid))
    if wedge == '' and quadid != '':
        QuadIdList.extend([quadid])

    folderlist = []
    for QuadId in QuadIdList:
        if 'HV0' in QuadId and hv == '':
            hv = 'HV0'
        if 'HV1' in QuadId and hv == '':
            hv = 'HV1'

        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_28_sTGC_QS1-HV0_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_27_sTGC_QS1-HV1_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.1' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS2_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_22_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_01_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_26_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.2' or 'WEDGE1-HVFILTER' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QS3_PIVOT-WEDGE1-HVFILTER_Layer4_S_GFZP2'])

        ##### WEDGE 2 #####
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_07_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_07_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QS1-HV1_PIVOT-WEDGE2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_28_sTGC_QS1_PIVOT-WEDGE2-HV0-WAIT2S_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QS1-HV1_PIVOT-WEDGE2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QS1-HV1_PIVOT-WEDGE2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QS1_PIVOT-WEDGE2-HV0_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_01_sTGC_QS1_PIVOT-WEDGE2-HV1-TRY2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_01_sTGC_QS1_PIVOT-WEDGE2-HV0-TRY4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.4' or 'WEDGE2' in wedge) and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_01_sTGC_QS1_PIVOT-WEDGE2-HV1-TRY2_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-WEDGE2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_07_sTGC_QS2_PIVOT-WEDGE2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_07_sTGC_QS2_PIVOT-WEDGE2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_01_sTGC_QS2_PIVOT-WEDGE2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-WEDGE2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-WEDGE2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-WEDGE2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_06_sTGC_QS2_PIVOT-WEDGE2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_06_sTGC_QS2_PIVOT-WEDGE2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_06_sTGC_QS2_PIVOT-WEDGE2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QS2_PIVOT-WEDGE2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.7' or 'WEDGE2' in wedge) and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QS2_PIVOT-WEDGE2_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_01_sTGC_QS3_PIVOT-WEDGE2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QS3_PIVOT-WEDGE2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QS3_PIVOT-WEDGE2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_PIVOT-WEDGE2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QS3_PIVOT-WEDGE2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_01_sTGC_QS3_PIVOT-WEDGE2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QS3_PIVOT-WEDGE2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QS3_PIVOT-WEDGE2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_PIVOT-WEDGE2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_PIVOT-WEDGE2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_01_sTGC_QS3_PIVOT-WEDGE2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.6' or 'WEDGE2' in wedge) and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QS3_PIVOT-WEDGE2_Layer4_S_GFZP2'])


#WEDGE3


        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV1_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_25_sTGC_QS1-HV0_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_PIVOT-8_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QS2_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_15_sTGC_QS2_PIVOT-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_15_sTGC_QS2_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QS2_PIVOT-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_24_sTGC_QS2_PIVOT-4-FIXED_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_25_sTGC_QS2_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_24_sTGC_QS2_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_15_sTGC_QS2_PIVOT-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_15_sTGC_QS2_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QS2_PIVOT-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QS2_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.4') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QS2_PIVOT-4_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_17_sTGC_QS3_PIVOT-DISHY_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_02_sTGC_QS3_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_02_sTGC_QS3_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_03_sTGC_QS3_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_02_sTGC_QS3_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_02_sTGC_QS3_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_02_sTGC_QS3_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_06_sTGC_QS3_PIVOT-8_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_07_sTGC_QS2_PIVOT-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_07_sTGC_QS2_PIVOT-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_08_sTGC_QS2_PIVOT-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_07_sTGC_QS2_PIVOT-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_07_sTGC_QS2_PIVOT-5_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_09_sTGC_QS2_PIVOT-10_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_09_sTGC_QS1-HV0_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_09_sTGC_QS1-HV1_PIVOT-10_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_15_sTGC_QS1-HV0_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_14_sTGC_QS1-HV0_PIVOT-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_15_sTGC_QS1-HV0_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_10_sTGC_QS1-HV0_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_10_sTGC_QS1-HV0_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_10_sTGC_QS1-HV0_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7-TRY2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_10_sTGC_QS1-HV0_PIVOT-7-TRY2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_14_sTGC_QS1-HV1_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_14_sTGC_QS1-HV0_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_10_sTGC_QS1-HV0_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_10_sTGC_QS1-HV1_PIVOT-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_16_sTGC_QS3_PIVOT-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_13_sTGC_QS2_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_13_sTGC_QS2_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_14_sTGC_QS2_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_14_sTGC_QS2_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_13_sTGC_QS2_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_14_sTGC_QS2_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.10') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_15_sTGC_QS2_CONFIRM-10_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_07_sTGC_QS2_PIVOT-6-FIXED_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_20_sTGC_QS2_PIVOT-6_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_04_sTGC_QS1-HV1_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_05_sTGC_QS1-HV0_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_05_sTGC_QS1-HV1_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_31_sTGC_QS1-HV0_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_29_sTGC_QS1-HV1_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV0_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV0_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_04_sTGC_QS1-HV1_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_04_sTGC_QS1-HV0_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_31_sTGC_QS1-HV1_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_31_sTGC_QS1-HV1_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_31_sTGC_QS1-HV0_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV0_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV0_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_03_sTGC_QS1-HV1_CONFIRM-1_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_05_sTGC_QS1-HV0_CONFIRM-1_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10-FIXGFZ_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_13_sTGC_QS3_PIVOT-10_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_17_sTGC_QS1-HV1_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_17_sTGC_QS1-HV0_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_20_sTGC_QS1-HV1_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_17_sTGC_QS1-HV1_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_17_sTGC_QS1-HV1_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_17_sTGC_QS1-HV0_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_20_sTGC_QS1-HV1_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV0_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_14_sTGC_QS1-HV1_PIVOT-9_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_20_sTGC_QS2_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_19_sTGC_QS2_PIVOT-8_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_21_sTGC_QS2_CONFIRM-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_26_sTGC_QS3_CONFIRM-3_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_04_sTGC_QS2_CONFIRM-9_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV1_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV0_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV1_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV0_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV1_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV1_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV0_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_01_sTGC_QS1-HV1_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_01_sTGC_QS1-HV1_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_01_sTGC_QS1-HV0_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV1_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV0_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV1_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV1_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_28_sTGC_QS1-HV0_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV1_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_08_sTGC_QS1-HV0_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_01_sTGC_QS1-HV0_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_02_sTGC_QS1-HV1_CONFIRM-2_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.2') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_02_sTGC_QS1-HV0_CONFIRM-2_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_09_sTGC_QS3_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_09_sTGC_QS3_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_09_sTGC_QS3_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_09_sTGC_QS3_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.2') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QS3_CONFIRM-2_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_26_sTGC_QS3_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_26_sTGC_QS3_CONFIRM-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_26_sTGC_QS3_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_31_sTGC_QS3_CONFIRM-4_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV0_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV1_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV0_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV1_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV0_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV1_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV0_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV0_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV1_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV0_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV1_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QS1-HV0_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV0_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV1_CONFIRM-3_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.3') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_05_sTGC_QS1-HV0_CONFIRM-3_Layer4_S_GFZP2'])

#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer1_S_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV0_CONFIRM-4_Layer1_S_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer1_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer2_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer2_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer2_S_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer2_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV0_CONFIRM-4_Layer2_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer3_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer3_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer3_S_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer3_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV0_CONFIRM-4_Layer3_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer4_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer4_P_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_12_sTGC_QS1-HV0_CONFIRM-4_Layer4_S_GFZP1'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer4_S_GFZP2'])
#        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
#            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_25_sTGC_QS3_PIVOT-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_25_sTGC_QS3_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS3_PIVOT-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS3_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QS3_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_25_sTGC_QS3_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_25_sTGC_QS3_PIVOT-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_07_sTGC_QS3_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_09_sTGC_QS3_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_07_sTGC_QS3_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_09_sTGC_QS3_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_08_sTGC_QS3_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_07_sTGC_QS3_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_06_sTGC_QS3_PIVOT-9_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QS3_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QS3_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QS3_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QS3_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QS3_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_16_sTGC_QS3_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QS3_PIVOT-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QS2_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QS2_CONFIRM-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_12_sTGC_QS2_CONFIRM-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QS2_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QS2_CONFIRM-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QS2_CONFIRM-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_16_sTGC_QS2_CONFIRM-15_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_25_sTGC_QL3_PIVOT-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_14_sTGC_QL3_PIVOT-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_14_sTGC_QL3_PIVOT-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_13_sTGC_QL3_PIVOT-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_15_sTGC_QL3_PIVOT-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_14_sTGC_QL3_PIVOT-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_14_sTGC_QL3_PIVOT-1_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL3_PIVOT-2_Layer4_S_GFZP2'])


        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer1_P_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer1_S_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer1_S_GFZP2'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer2_P_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_27_sTGC_QL3_PIVOT-3_Layer2_S_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer2_S_GFZP2'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer4_P_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer3_S_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer3_S_GFZP2'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer4_P_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer4_S_GFZP1'])
        #if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QL3_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_13_sTGC_QL3_PIVOT-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.4') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_18_sTGC_QL3_PIVOT-4_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_23_sTGC_QS1-HV0_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_23_sTGC_QS1-HV1_CONFIRM-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_17_sTGC_QS1-HV0_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV1_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_20_sTGC_QS1-HV0_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_12_sTGC_QS1-HV0_CONFIRM-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV1_CONFIRM-4_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.4') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_13_sTGC_QS1-HV0_CONFIRM-4_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_PIVOT-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_PIVOT-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_PIVOT-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_PIVOT-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_20_sTGC_QL2_PIVOT-1_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_02_sTGC_QL2_PIVOT-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_02_sTGC_QL2_PIVOT-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_02_sTGC_QL2_PIVOT-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QL2_PIVOT-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_02_sTGC_QL2_PIVOT-2_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.10') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_15_sTGC_QS3_CONFIRM-10_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_16_sTGC_QS1-HV1_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV0_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV1_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV0_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV1_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV1_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_09_sTGC_QS1-HV0_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV1_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV1_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV0_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV1_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV0_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV1_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV1_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV0_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV1_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_08_sTGC_QS1-HV0_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV0_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV1_CONFIRM-6_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.6') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_07_sTGC_QS1-HV0_CONFIRM-6_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_23_sTGC_QS2_CONFIRM-0_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_18_sTGC_QS2_CONFIRM-0_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_18_sTGC_QS2_CONFIRM-0_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_18_sTGC_QS2_CONFIRM-0_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_18_sTGC_QS2_CONFIRM-0_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.0') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-0_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QS2_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_22_sTGC_QS2_CONFIRM-11_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_07_sTGC_QS3_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_07_sTGC_QS3_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_07_sTGC_QS3_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_07_sTGC_QS3_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.11') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS3_CONFIRM-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_14_sTGC_QS2_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS2_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_14_sTGC_QS2_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS2_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS2_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_14_sTGC_QS2_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS2_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_14_sTGC_QS2_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS2_CONFIRM-12_Layer4_S_GFZP2'])


        """
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_19_sTGC_QS2_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_19_sTGC_QS2_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-14_Layer4_S_GFZP2'])
        """


        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV0_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_16_sTGC_QL1-HV1_PIVOT-9_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV1_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_08_sTGC_QS1-HV0_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_07_sTGC_QS1-HV0_CONFIRM-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_07_sTGC_QS1-HV1_CONFIRM-5_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.5') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_07_sTGC_QS1-HV0_CONFIRM-5_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QS3_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QS3_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QS3_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QS3_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.12') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_09_sTGC_QS3_CONFIRM-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV0_PIVOT-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_10_sTGC_QL1-HV1_PIVOT-5_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV1_CONFIRM-7_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.7') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_15_sTGC_QS1-HV0_CONFIRM-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV1_CONFIRM-8_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.8') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_22_sTGC_QS1-HV0_CONFIRM-8_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer1_S_GFZP2'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer2_P_GFZP1'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer2_S_GFZP1'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer2_S_GFZP2'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer3_P_GFZP1'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer3_S_GFZP2'])
        #if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_07_sTGC_QS3_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.8') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_16_sTGC_QS3_CONFIRM-8_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QL2_PIVOT-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QL2_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QL2_PIVOT-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-4_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer1_S_GFZP2'])
        #TMP this layer will need an additional external resistor because one of the wire groups has 0 resistence to the HV line. To id bad channels, use the run without a resistor
        #TMP if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #TMP    folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3-RESISTOR_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_13_sTGC_QL2_PIVOT-3_Layer4_S_GFZP2'])


#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-3_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_27_sTGC_QL3_PIVOT-3_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer1_S_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer1_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer2_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer2_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_27_sTGC_QL3_PIVOT-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer2_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer2_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer3_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer3_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer3_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer4_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_23_sTGC_QL3_PIVOT-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer4_S_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QL3_PIVOT-3_Layer4_S_GFZP2'])
#        if (wedge=='' or wedge=='QL3.P.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Aug_28_sTGC_QL3_PIVOT-3_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_24_sTGC_QS3_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QS3_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QS3_CONFIRM-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_24_sTGC_QS3_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QS3_CONFIRM-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QS3_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QS3_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QS3_CONFIRM-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QS3_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_24_sTGC_QS3_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QS3_CONFIRM-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.7') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_23_sTGC_QS3_CONFIRM-7_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QL3_PIVOT-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QL3_PIVOT-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QL1-HV1_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV0_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QL1-HV1_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QL1-HV0_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_30_sTGC_QL1-HV1_PIVOT-12_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV1_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV0_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV0_PIVOT-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV1_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV1_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV0_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV1_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV1_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_31_sTGC_QL1-HV0_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_04_sTGC_QL1-HV1_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_04_sTGC_QL1-HV0_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV1_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV1_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV0_PIVOT-10_Layer3_S_GFZP2'])
#        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
#            folderlist.extend(['Feb_03_sTGC_QL1-HV1_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_04_sTGC_QL1-HV1_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_04_sTGC_QL1-HV0_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV1_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV0_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_03_sTGC_QL1-HV1_PIVOT-10_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL2_PIVOT-5_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_05_sTGC_QL3_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_31_sTGC_QL3_PIVOT-9_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.6') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_06_sTGC_QS3_CONFIRM-6_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV1_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV0_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV0_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV1_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV0_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV0_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV1_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV0_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV0_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV1_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QL1-HV0_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV0_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV1_CONFIRM-2_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_13_sTGC_QL1-HV0_CONFIRM-2_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_12_sTGC_QL2_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QL2_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_12_sTGC_QL2_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_16_sTGC_QL2_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QL2_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_12_sTGC_QL2_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_16_sTGC_QL2_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QL2_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_12_sTGC_QL2_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_16_sTGC_QL2_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QL2_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.1') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_12_sTGC_QL2_CONFIRM-1_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_11_sTGC_QS2_PIVOT-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.11') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QS2_PIVOT-11_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV0_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_14_sTGC_QS1-HV1_PIVOT-12_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_17_sTGC_QL3_CONFIRM-6_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3-RUN2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QL1-HV0_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3-RUN2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV0_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QL1-HV1_CONFIRM-3_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.3') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QL1-HV0_CONFIRM-3_Layer4_S_GFZP2'])







        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.2') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_20_sTGC_QL3_CONFIRM-2_Layer4_S_GFZP2'])





        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13-TRY2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13-TRY2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_PIVOT-13_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.4') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_21_sTGC_QL2_CONFIRM-4_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1-RUN2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1-RUN2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_CONFIRM-1_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.1') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_CONFIRM-1_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL3_CONFIRM-5-RUN3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL3_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5-RUN3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL3_CONFIRM-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL3_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL3_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_26_sTGC_QL3_CONFIRM-5_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QL2_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QL2_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QL2_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_28_sTGC_QL2_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.2') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_27_sTGC_QL2_CONFIRM-2_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6-RUN2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6-RUN2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QL2_PIVOT-6_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_05_sTGC_QL1-HV0_PIVOT-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_05_sTGC_QL1-HV1_PIVOT-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_05_sTGC_QL1-HV0_PIVOT-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6-RUN3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6-RUN2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6-RUN2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV0_PIVOT-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_06_sTGC_QL1-HV1_PIVOT-6_Layer4_S_GFZP2'])


#        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8-TRY2_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8-TRY3_Layer1_P_GFZP1'])
#        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
#            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8-TRY4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_09_sTGC_QL3_PIVOT-8_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6-RUN2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.6') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_10_sTGC_QL3_PIVOT-6_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_11_sTGC_QL2_PIVOT-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV0_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.13') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_11_sTGC_QL1-HV1_PIVOT-13_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_13_sTGC_QL3_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_13_sTGC_QL3_CONFIRM-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_13_sTGC_QL3_CONFIRM-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_13_sTGC_QL3_CONFIRM-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.7') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_28_sTGC_QL3_CONFIRM-7_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_02_sTGC_QL2_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_02_sTGC_QL2_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.3') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_29_sTGC_QL2_CONFIRM-3_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV1_CONFIRM-7_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.7') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_02_sTGC_QL1-HV0_CONFIRM-7_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.8') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_08_sTGC_QL3_CONFIRM-8_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV1_CONFIRM-10_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.10') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QL1-HV0_CONFIRM-10_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.5') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_09_sTGC_QL2_CONFIRM-5_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.3') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_11_sTGC_QL3_CONFIRM-3_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.6') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_16_sTGC_QL2_CONFIRM-6_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_22_sTGC_QL1-HV1_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_22_sTGC_QL1-HV0_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_22_sTGC_QL1-HV1_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_22_sTGC_QL1-HV1_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_22_sTGC_QL1-HV0_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV1_CONFIRM-8_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_23_sTGC_QL1-HV0_CONFIRM-8_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_07_sTGC_QL3_PIVOT-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_08_sTGC_QL3_PIVOT-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_07_sTGC_QL3_PIVOT-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.5') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_07_sTGC_QL3_PIVOT-5_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_22_sTGC_QL3_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.1') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_21_sTGC_QL3_CONFIRM-1_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_27_sTGC_QS2_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_27_sTGC_QS2_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.13') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jul_24_sTGC_QS2_PIVOT-13_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV0_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV0_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV1_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV0_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV0_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV1_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV1_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV0_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV0_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV1_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV0_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV0_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV1_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_22_sTGC_QL1-HV0_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_23_sTGC_QL1-HV1_PIVOT-14_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_04_sTGC_QL2_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_PIVOT-8_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_06_sTGC_QL2_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_06_sTGC_QL2_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_06_sTGC_QL2_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.8') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_05_sTGC_QL2_CONFIRM-8_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV0_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_14_sTGC_QS1-HV1_PIVOT-14_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_19_sTGC_QL1-HV1_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV0_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV1_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV0_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV1_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV1_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV0_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV1_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV1_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_07_sTGC_QL1-HV0_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV1_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV0_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV1_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV1_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV0_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV1_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV0_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV0_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV1_CONFIRM-11_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_06_sTGC_QL1-HV0_CONFIRM-11_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.11') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_26_sTGC_QL3_CONFIRM-11_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_24_sTGC_QL2_CONFIRM-7_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QL2_CONFIRM-7_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_24_sTGC_QL2_CONFIRM-7_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_21_sTGC_QL2_CONFIRM-7_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_20_sTGC_QL2_CONFIRM-7_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.7') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Aug_25_sTGC_QL2_CONFIRM-7_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV1_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV0_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_10_sTGC_QL1-HV1_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_10_sTGC_QL1-HV0_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_10_sTGC_QL1-HV1_CONFIRM-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV1_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV0_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_10_sTGC_QL1-HV1_CONFIRM-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV1_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV0_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_03_sTGC_QL1-HV1_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_03_sTGC_QL1-HV0_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_11_sTGC_QL1-HV1_CONFIRM-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV1_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV0_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV1_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV0_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jul_30_sTGC_QL1-HV0_CONFIRM-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_03_sTGC_QL1-HV1_CONFIRM-5BIS_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.5') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_03_sTGC_QL1-HV0_CONFIRM-5_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV1_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV0_PIVOT-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV0_PIVOT-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV1_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV0_PIVOT-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV0_PIVOT-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV1_PIVOT-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV1_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_03_sTGC_QL1-HV0_PIVOT-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV0_PIVOT-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_07_sTGC_QL1-HV1_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_07_sTGC_QL1-HV0_PIVOT-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV0_PIVOT-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV0_PIVOT-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.8') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_04_sTGC_QL1-HV1_PIVOT-8_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_07_sTGC_QL3_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_07_sTGC_QL3_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_07_sTGC_QL3_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_08_sTGC_QL3_CONFIRM-10_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV0_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV0_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV0_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV1_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_08_sTGC_QL1-HV0_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV1_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV0_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV1_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV1_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV0_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV1_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV0_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV0_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV1_CONFIRM-14_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.14') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_09_sTGC_QL1-HV0_CONFIRM-14_Layer4_S_GFZP2'])






        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV0_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Sep_11_sTGC_QL1-HV1_PIVOT-4_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV1_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV0_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV1_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV1_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV0_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV1_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_27_sTGC_QL1-HV0_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV1_CONFIRM-9_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.9') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Aug_28_sTGC_QL1-HV0_CONFIRM-9_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_24_sTGC_QL2_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Sep_23_sTGC_QL2_CONFIRM-9_Layer4_S_GFZP2'])




        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.2') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_14_sTGC_QS2_CONFIRM-2_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_14_sTGC_QS1-HV0_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV1_CONFIRM-9_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.9') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_15_sTGC_QS1-HV0_CONFIRM-9_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_08_sTGC_QL3_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_08_sTGC_QL3_PIVOT-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_08_sTGC_QL3_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_12_sTGC_QL3_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_19_sTGC_QL3_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.10') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_09_sTGC_QL3_PIVOT-10_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_06_sTGC_QS3_PIVOT-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_06_sTGC_QS3_PIVOT-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.3') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_01_sTGC_QS3_PIVOT-3_Layer4_S_GFZP2'])



        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.9') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_13_sTGC_QL3_CONFIRM-9_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10-TRY2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_21_sTGC_QL2_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_CONFIRM-10_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.9') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_20_sTGC_QL2_PIVOT-9_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_23_sTGC_QS1-HV0_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV0_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.15') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_22_sTGC_QS1-HV1_PIVOT-15_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_23_sTGC_QS2_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_23_sTGC_QS2_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_23_sTGC_QS2_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.15') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS2_PIVOT-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_28_sTGC_QS3_PIVOT-13_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4      _Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_26_sTGC_QS3_PIVOT-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4     _Layer4_P_GFZP1'])
        #if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4-RUN2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.4') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Oct_27_sTGC_QS3_PIVOT-4_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV0_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_29_sTGC_QL1-HV1_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_02_sTGC_QL1-HV1_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_02_sTGC_QL1-HV0_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV0_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Oct_30_sTGC_QL1-HV1_PIVOT-16_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_02_sTGC_QS3_CONFIRM-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS3_CONFIRM-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS3_CONFIRM-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS3_CONFIRM-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS3_CONFIRM-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_03_sTGC_QS3_CONFIRM-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.16') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_04_sTGC_QS2_PIVOT-16_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV1_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV0_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV0_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV1_CONFIRM-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV1_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV1_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV0_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_05_sTGC_QL1-HV1_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV1_CONFIRM-17_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.17') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_06_sTGC_QL1-HV0_CONFIRM-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_06_sTGC_QS2_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QS2_CONFIRM-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QL3_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QL3_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QL3_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QL3_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_09_sTGC_QL3_PIVOT-14_Layer4_S_GFZP2'])
 
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_10_sTGC_QL3_CONFIRM-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_12_sTGC_QS3_CONFIRM-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.14') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_13_sTGC_QS3_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.15') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.16') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_16_sTGC_QS3_PIVOT-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_17_sTGC_QS2_CONFIRM-18_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.20') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_PIVOT-20_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.1') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_18_sTGC_QS2_CONFIRM-1_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_23_sTGC_QL2_PIVOT-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_23_sTGC_QL2_PIVOT-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_23_sTGC_QL2_PIVOT-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_20_sTGC_QL2_PIVOT-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_23_sTGC_QL2_PIVOT-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.10') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_23_sTGC_QL2_PIVOT-10_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_24_sTGC_QS1-HV0_PIVOT-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_24_sTGC_QS1-HV1_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_24_sTGC_QS1-HV0_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_24_sTGC_QS1-HV0_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV0_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.17') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Nov_23_sTGC_QS1-HV1_PIVOT-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_26_sTGC_QS2_PIVOT-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_26_sTGC_QS2_PIVOT-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_26_sTGC_QS2_PIVOT-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_26_sTGC_QS2_PIVOT-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_26_sTGC_QS2_PIVOT-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.9') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_25_sTGC_QS2_PIVOT-9_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QS2_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QS2_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QS2_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_CONFIRM-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QL3_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QL3_CONFIRM-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QL3_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QL3_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_01_sTGC_QL3_CONFIRM-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QL3_CONFIRM-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QS2_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.17') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_02_sTGC_QS2_PIVOT-17_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QL3_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_03_sTGC_QL3_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QL3_PIVOT-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.3') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_04_sTGC_QS2_CONFIRM-3_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_09_sTGC_QL2_CONFIRM-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_10_sTGC_QL2_PIVOT-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_10_sTGC_QL2_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_10_sTGC_QL2_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_10_sTGC_QL2_PIVOT-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.11') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Dec_11_sTGC_QL2_PIVOT-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_30_sTGC_QS2_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.12') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Nov_27_sTGC_QS2_PIVOT-12_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_CONFIRM-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_CONFIRM-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_CONFIRM-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV1_CONFIRM-16_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.16') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_11_sTGC_QL1-HV0_CONFIRM-16_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV0_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_PIVOT-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV0_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV0_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_PIVOT-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV1_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_14_sTGC_QL1-HV0_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV0_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Dec_15_sTGC_QL1-HV1_PIVOT-18_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.5') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_05_sTGC_QS2_CONFIRM-5_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.13') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_08_sTGC_QL3_CONFIRM-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.14') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_12_sTGC_QL3_CONFIRM-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_CONFIRM-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_15_sTGC_QS2_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_15_sTGC_QS2_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_15_sTGC_QS2_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_14_sTGC_QS2_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.18') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_15_sTGC_QS2_PIVOT-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_CONFIRM-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer3_S_GFZP2'])
        #if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
        #    folderlist.extend(['Jan_20_sTGC_QL3_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL3_PIVOT-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_21_sTGC_QL2_PIVOT-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.12') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_22_sTGC_QL2_PIVOT-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_25_sTGC_QL2_CONFIRM-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QS1-HV1_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QS1-HV0_CONFIRM-10_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer2_S_GFZP1'])
        #if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
        #    folderlist.extend(['Jan_25_sTGC_QS1-HV1_CONFIRM-10_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV1_CONFIRM-10_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.10') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QS1-HV0_CONFIRM-10_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer2_S_GFZP2'])
        #if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
        #    folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15RUN2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV0_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_27_sTGC_QL1-HV1_PIVOT-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV1_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV0_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV1_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV1_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV0_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV1_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_26_sTGC_QL1-HV0_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV1_CONFIRM-12_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.12') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_25_sTGC_QL1-HV0_CONFIRM-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_27_sTGC_QS3_PIVOT-18_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_29_sTGC_QS3_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_29_sTGC_QS3_PIVOT-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_29_sTGC_QS3_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_29_sTGC_QS3_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.P.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jan_28_sTGC_QS3_PIVOT-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV0_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.16') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jan_29_sTGC_QS1-HV1_PIVOT-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_02_sTGC_QS1-HV1_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_02_sTGC_QS1-HV0_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_02_sTGC_QS1-HV1_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_02_sTGC_QS1-HV0_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV0_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.19') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_01_sTGC_QS1-HV1_PIVOT-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.15') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_02_sTGC_QL3_PIVOT-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_PIVOT-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16`_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16`_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_03_sTGC_QL3_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.16') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_04_sTGC_QL3_CONFIRM-16_Layer4_S_GFZP2'])






        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV0_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.11') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_05_sTGC_QL1-HV1_PIVOT-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV1_CONFIRM-6_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.6') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_10_sTGC_QL1-HV0_CONFIRM-6_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.13') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_12_sTGC_QL2_PIVOT-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_15_sTGC_QL2_CONFIRM-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.14') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_16_sTGC_QL2_PIVOT-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV1_CONFIRM-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV0_CONFIRM-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV0_CONFIRM-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV0_CONFIRM-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV1_CONFIRM-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV0_CONFIRM-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV0_CONFIRM-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV0_CONFIRM-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV1_CONFIRM-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QL1-HV0_CONFIRM-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV0_CONFIRM-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV1_CONFIRM-19_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_12_sTGC_QL1-HV0_CONFIRM-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV1_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV1_PIVOT-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV1_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_18_sTGC_QS1-HV0_PIVOT-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV1_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV0_PIVOT-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV1_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV0_PIVOT-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_17_sTGC_QS1-HV1_PIVOT-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.18') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_18_sTGC_QS3_CONFIRM-18_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV1_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV0_CONFIRM-11_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV1_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV0_CONFIRM-11_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV1_CONFIRM-11_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV0_CONFIRM-11_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV1_CONFIRM-11_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.11') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_20_sTGC_QS1-HV0_CONFIRM-11_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV0_PIVOT-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV1_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_19_sTGC_QS1-HV0_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer2_S_GFZP2'])
        #if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
        #    folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18NEWFREQ_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV0_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.18') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_22_sTGC_QS1-HV1_PIVOT-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_22_sTGC_QS3_CONFIRM-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_22_sTGC_QS3_CONFIRM-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_22_sTGC_QS3_CONFIRM-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_22_sTGC_QS3_CONFIRM-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_22_sTGC_QS3_CONFIRM-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.19') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_23_sTGC_QS3_CONFIRM-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.17') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Feb_25_sTGC_QL3_PIVOT-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_PIVOT-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV0_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_PIVOT-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV1_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_25_sTGC_QL1-HV0_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV0_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV0_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV0_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV0_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.19') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Feb_26_sTGC_QL1-HV1_PIVOT-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV1_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV0_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV0_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV0_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV1_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV0_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV0_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV0_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV1_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_02_sTGC_QL1-HV0_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV0_CONFIRM-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV1_CONFIRM-15_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.15') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_01_sTGC_QL1-HV0_CONFIRM-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_CONFIRM-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_03_sTGC_QL3_PIVOT-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.18') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QL3_PIVOT-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.19') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_PIVOT-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.6') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_04_sTGC_QS2_CONFIRM-6_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.1') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-1_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV0_CONFIRM-51_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV0_CONFIRM-51_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV0_CONFIRM-51_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV1_CONFIRM-51_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV0_CONFIRM-51_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV1_CONFIRM-51_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_09_sTGC_QS1-HV0_CONFIRM-51_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV1_CONFIRM-51_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV1_CONFIRM-51_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV0_CONFIRM-51_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV1_CONFIRM-51_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV0_CONFIRM-51_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV0_CONFIRM-51_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV1_CONFIRM-51_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.51') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_08_sTGC_QS1-HV0_CONFIRM-51_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV0_PIVOT-20_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.20') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_12_sTGC_QL1-HV1_PIVOT-20_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QL2_PIVOT-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QL2_PIVOT-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QL2_PIVOT-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QL2_PIVOT-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_12_sTGC_QL2_PIVOT-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QL2_PIVOT-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.13') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_05_sTGC_QS3_CONFIRM-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.17') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_15_sTGC_QS3_CONFIRM-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.15') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_16_sTGC_QL2_CONFIRM-15_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_19_sTGC_QL1-HV1_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_19_sTGC_QL1-HV0_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV1_CONFIRM-18_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.18') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_18_sTGC_QL1-HV0_CONFIRM-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_23_sTGC_QL3_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_23_sTGC_QL3_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.12') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_23_sTGC_QL3_CONFIRM-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_24_sTGC_QL3_CONFIRM-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.C.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_25_sTGC_QL3_CONFIRM-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS3.C.9') and ( 'QS3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_29_sTGC_QS3_CONFIRM-9_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV0_PIVOT-2_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.P.2') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Mar_30_sTGC_QL1-HV1_PIVOT-2_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_31_sTGC_QS2_CONFIRM-8_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_31_sTGC_QS2_CONFIRM-8_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_06_sTGC_QS2_CONFIRM-8_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_31_sTGC_QS2_CONFIRM-8_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_06_sTGC_QS2_CONFIRM-8_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Mar_31_sTGC_QS2_CONFIRM-8_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.C.8') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_05_sTGC_QS2_CONFIRM-8_Layer4_S_GFZP2'])

   
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV1_CONFIRM-4_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL1.C.4') and ( 'QL1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_06_sTGC_QL1-HV0_CONFIRM-4_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL2_CONFIRM-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL3_PIVOT-19_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL3_PIVOT-19_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL3_PIVOT-19_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL3_PIVOT-19_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_13_sTGC_QL3_PIVOT-19_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.19') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_14_sTGC_QL3_PIVOT-19_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL3.P.1-RESURRECTED') and ( 'QL3' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_16_sTGC_QL3_PIVOT-1-RESURRECTED!!_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL2_PIVOT-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_PIVOT-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL2_PIVOT-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_19_sTGC_QL2_PIVOT-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.16') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_20_sTGC_QL2_CONFIRM-16_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV1_CONFIRM-13_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.13') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_21_sTGC_QS1-HV0_CONFIRM-13_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_CONFIRM-12_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_CONFIRM-12_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_CONFIRM-12_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_CONFIRM-12_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_CONFIRM-12_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_CONFIRM-12_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_CONFIRM-12_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_CONFIRM-12_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_CONFIRM-12_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV1_CONFIRM-12_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.12') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_26_sTGC_QS1-HV0_CONFIRM-12_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_28_sTGC_QS1-HV1_PIVOT-20_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_28_sTGC_QS1-HV0_PIVOT-20_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_28_sTGC_QS1-HV1_PIVOT-20_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV0_PIVOT-20_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.20') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_27_sTGC_QS1-HV1_PIVOT-20_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.P.17') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Apr_28_sTGC_QL2_PIVOT-17_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV0_PIVOT-1_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV0_PIVOT-1_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV1_PIVOT-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV0_PIVOT-1_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV0_PIVOT-1_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV0_PIVOT-1_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV0_PIVOT-1_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV1_PIVOT-1_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV1_PIVOT-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV0_PIVOT-1_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV1_PIVOT-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_29_sTGC_QS1-HV0_PIVOT-1_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV0_PIVOT-1_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.1') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Apr_30_sTGC_QS1-HV1_PIVOT-1_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_12_sTGC_QL2_CONFIRM-18_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_12_sTGC_QL2_CONFIRM-18_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_12_sTGC_QL2_CONFIRM-18_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QL2.C.18') and ( 'QL2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['May_17_sTGC_QL2_CONFIRM-18_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['May_20_sTGC_QS1-HV0_PIVOT-21_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.21') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['May_20_sTGC_QS1-HV1_PIVOT-21_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-53_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.53') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-53_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QS1-HV1_CONFIRM-52_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QS1-HV0_CONFIRM-52_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QS1-HV1_CONFIRM-52_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_09_sTGC_QS1-HV1_CONFIRM-52_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_09_sTGC_QS1-HV0_CONFIRM-52_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV1_CONFIRM-52_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.52') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_08_sTGC_QS1-HV0_CONFIRM-52_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV1_CONFIRM-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV1_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV0_CONFIRM-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV1_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_10_sTGC_QS1-HV0_CONFIRM-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV0_CONFIRM-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV1_CONFIRM-14_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.14') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_11_sTGC_QS1-HV0_CONFIRM-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS2.P.14') and ( 'QS2' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == '' ):
            folderlist.extend(['Jun_22_sTGC_QS2_PIVOT-14_Layer4_S_GFZP2'])

        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV1_PIVOT-22_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV1_PIVOT-22_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer2_S_GFZP2'])
        #if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
        #    folderlist.extend(['Jun_24_sTGC_QS1-HV1_PIVOT-22_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV0_PIVOT-22_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV0_PIVOT-22_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV1_PIVOT-22_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV1_PIVOT-22_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_24_sTGC_QS1-HV0_PIVOT-22_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV0_PIVOT-22_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.P.22') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_PIVOT-22_Layer4_S_GFZP2'])


        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_CONFIRM-54_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV0_CONFIRM-54_Layer1_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer1_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==1 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer1_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer2_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_CONFIRM-54_Layer2_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV1_CONFIRM-54_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==2 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_25_sTGC_QS1-HV0_CONFIRM-54_Layer2_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer3_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer3_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==3 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer3_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'P' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer4_P_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P1' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer4_S_GFZP1'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV1' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV1_CONFIRM-54_Layer4_S_GFZP2'])
        if (wedge=='' or wedge=='QS1.C.54') and ( 'QS1' in QuadId ) and ( layer == '' or layer==4 ) and ( ab == '' or ab == 'S' ) and ( gfz == '' or gfz == 'P2' ) and ( hv == '' or hv == 'HV0' ):
            folderlist.extend(['Jun_26_sTGC_QS1-HV0_CONFIRM-54_Layer4_S_GFZP2'])
    return folderlist
