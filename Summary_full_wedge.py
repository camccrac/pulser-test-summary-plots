import csv
import os
import sys
import tkinter as tk
import pickle

import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import WedgeDB
from foldernameDB import getFoldersList
from QABmap import Qmap
from exception_list import exceptions

fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(10, 14))


def get_wedge(num=None):
    if num is None:
        # launch tk window
        window = tk.Tk()
        window.geometry('500x150')
        window.title('Full Wedge')

        # helper function for what to do with on-screen button
        def show_values():
            wedgeNum.get()
            window.destroy()

        # frame containing "Wedge Number:" input
        frm1 = tk.Frame(master=window)
        frm1.grid(padx=100, pady=50)
        wedgeNum = tk.IntVar()
        lbl = tk.Label(master=frm1, text='Wedge Number: ')
        ent = tk.Entry(master=frm1, textvariable=wedgeNum)
        bttn = tk.Button(master=frm1, text='GO!', command=show_values)

        lbl.grid(row=0, column=0)
        ent.grid(row=0, column=1)
        bttn.grid(row=0, column=2)

        # display window
        window.mainloop()

        # get what's in the text box once button pressed
        wedgeNum = wedgeNum.get()
    else:
        wedgeNum = num
    quads = WedgeDB.wedges[wedgeNum].quads

    # separate the wedge into individual quads
    quad1 = quads[0]
    quad2 = quads[1]
    quad3 = quads[2]

    # determine if the wedge is pivot/confirm
    if 'P' in quads[0]:
        P_or_C = 'PIVOT'
    else:
        P_or_C = 'CONFIRM'

    # collect their corresponding stgc
    stgc1 = quad1[0:3]
    stgc2 = quad2[0:3]
    stgc3 = quad3[0:3]

    # collect their unique ID numbers
    quad1 = quad1[::-1]
    uid1 = quad1[0:int(quad1.index('.'))]
    uid1 = P_or_C + '-' + str(uid1[::-1]) + '_'

    quad2 = quad2[::-1]
    uid2 = quad2[0:int(quad2.index('.'))]
    uid2 = P_or_C + '-' + str(uid2[::-1]) + '_'

    quad3 = quad3[::-1]
    uid3 = quad3[0:int(quad3.index('.'))]
    uid3 = P_or_C + '-' + str(uid3[::-1]) + '_'

    if 'P' in quads[0]:
        PC = 'P'
    else:
        PC = 'C'

    # weird shit because of one-off naming conventions etc
    if wedgeNum == 1:
        uid1 = 'PIVOT-WEDGE1'
        uid2 = 'PIVOT-WEDGE1'
        uid3 = 'PIVOT-WEDGE1'
    elif wedgeNum == 2:
        uid1 = 'PIVOT-WEDGE2'
        uid2 = 'PIVOT-WEDGE2'
        uid3 = 'PIVOT-WEDGE2'
    elif wedgeNum == 3:
        uid3 = 'PIVOT-DISHY'

    return stgc1, stgc2, stgc3, uid1, uid2, uid3, PC


def full_wedge_plot(wedgeNum, save=False):
    """
    Makes a plot of Amplitude vs pad/strip/wire number, for a whole wedge,
    specified by wedgeNum (int between 1 and 70).
    """
    global fig, ax1, ax2, ax3
    pad_data = {}
    wire_data = {}
    strip_data = {}

    def db_path(folder):
        """Given folder name, return database.csv file path"""
        return f'data/{folder}/database.csv'

    # get info about the wedge
    # quadtype = 'QS3' or 'QL2'-ish string
    # uid = unique ID, like the 2 in QS1.P.2
    # PC = "P" or "C" for pivot or confirm
    quadtype1, quadtype2, quadtype3, uid1, uid2, uid3, PC = get_wedge(wedgeNum)

    # get the list of folders containing data from each quad
    folderList1_pads = getFoldersList('', quadtype1, '', 'P', 'P1', "")
    folderList2_pads = getFoldersList('', quadtype2, '', 'P', 'P1', "")
    folderList3_pads = getFoldersList('', quadtype3, '', 'P', 'P1', "")
    folderList1_strips = getFoldersList('', quadtype1, '', 'S', '', "")
    folderList2_strips = getFoldersList('', quadtype2, '', 'S', '', "")
    folderList3_strips = getFoldersList('', quadtype3, '', 'S', '', "")

    # sort files into different categories
    # e.g. one for HV0 Strip P2 files, one for Q3 Pad P1s
    Q1_pads = [db_path(f) for f in folderList1_pads if uid1 in f]
    Q2_pads = [db_path(f) for f in folderList2_pads if uid2 in f]
    Q3_pads = [db_path(f) for f in folderList3_pads if uid3 in f]
    Q1_strips = [db_path(f) for f in folderList1_strips if uid1 in f]
    Q2_strips = [db_path(f) for f in folderList2_strips if uid2 in f]
    Q3_strips = [db_path(f) for f in folderList3_strips if uid3 in f]
    HV0_p1 = [f for f in Q1_pads if "HV0" in f]
    HV1_p1 = [f for f in Q1_pads if "HV1" in f]
    HV0 = [f for f in Q1_strips if "HV0" in f]
    HV1 = [f for f in Q1_strips if "HV1" in f]
    HV0_sp1 = [f for f in HV0 if 'P1' in f]
    HV0_sp2 = [f for f in HV0 if 'P2' in f]
    HV1_sp1 = [f for f in HV1 if 'P1' in f]
    HV1_sp2 = [f for f in HV1 if 'P2' in f]
    Q2_sp1 = [f for f in Q2_strips if 'P1' in f]
    Q2_sp2 = [f for f in Q2_strips if 'P2' in f]
    Q3_sp1 = [f for f in Q3_strips if 'P1' in f]
    Q3_sp2 = [f for f in Q3_strips if 'P2' in f]

    # a couple variables to be updated as we plot things,
    # to control where on the x axis we start plotting Q2 and Q3 data
    max_chan = {'strip': 0, 'pad': 0, 'wire': 0}
    prev_max = {'strip': 0, 'pad': 0, 'wire': 0}

    # initialize plot
    wedge = WedgeDB.wedges[wedgeNum]
    title = (f'{wedge.sectorName} / {wedge.alias} / Wedge {wedge.number} ' +
             f'({wedge.quads[0]}, {wedge.quads[1]}, {wedge.quads[2]})')
    fig.suptitle(title, fontsize=16)
    # colours corresponding to layers
    cmap = {1: 'blue', 2: 'red', 3: 'gold', 4: 'green'}

    def plot(db_file_list, quadtype, drawline, pintype):
        """
        Plots data on the figure initialized above.

        Inputs:
        - folderlist: list of strings, folders with database.csv files
        - quadtype: things like 'QS1' or 'QL3'
        - drawline: bool, whether or not to draw a line (start of new quad)
        - pintype: string, one of 'S' (strip GFZ) or 'P' (pad/wire GFZ)
        """
        nonlocal max_chan, prev_max, cmap, pad_data, wire_data, strip_data
        # draw vertical lines if necessary
        if drawline:
            if pintype == 'S':
                ax1.axvline(max_chan['strip'], ls='--', color='grey')
            else:
                ax2.axvline(max_chan['pad'], ls='--', color='grey')
                ax3.axvline(max_chan['wire'], ls='--', color='grey')
            prev_max = max_chan.copy()

        """
        A clarification about variable names:

        In this code, we call the number of strip/pad/wire the "spw number".
        (spw = strip pad wire, note this is different than a gfz channel,
        like pad 7 may correspond to gfz p1 channel 112 or something)

        We also call the string 'strip', 'pad', or 'wire', the "kind" of a pin.
        """
        # lists to store info from files before plotting, dict keys = layers
        spws = {1: [], 2: [], 3: [], 4: []}
        kinds = {1: [], 2: [], 3: [], 4: []}
        masks = {1: [], 2: [], 3: [], 4: []}
        # layers in each file, from file names
        layers = [int(d[d.index('Layer')+len('Layer')]) for d in db_file_list]
        # get data
        for db, layer in zip(db_file_list, layers):
            # what kind of gfz is this?
            gfz = 'P2' if (pintype == 'S') and ('P2' in db) else 'P1'
            # gfz channels: 1st column in database.csv file
            channels = np.loadtxt(db, delimiter=', ,', skiprows=1, usecols=(0))
            # amplitudes = 3rd column
            amps = np.loadtxt(db, delimiter=', ,', skiprows=1, usecols=(2))
            # for some files there is only one entry, cast 0D array as list
            if channels.shape == ():
                channels = [channels]
            if amps.shape == ():
                amps = [amps]
            # get spw numbers and kinds
            for ch in channels:
                # use Qmap function (idk why it's called that)
                # to find spw number and kind of our given channel

                # skip a couple channels that were physically soldered to other
                # channels, but still "exist" in the data

                # also skip specific channels in files that had errors
                # e.g. accidentally clicked Layer 2 rather than 3
                # and have one extra channel that shouldn't exist
                # (exceptions are imported from separate file)
                if quadtype == "QL1" and pintype == "P" and ch in [198, 229]:
                    spw = -1
                    mask = True
                elif [db.split('/')[1], ch] in exceptions:
                    if pintype == "S":
                        kind = 'strip'
                    else:
                        if ch > 128:  # TODO: double check 128
                            kind = 'wire'
                        else:
                            kind = 'pad'
                    spw = -1
                    mask = True
                else:
                    spw, kind = Qmap(layer, pintype, gfz, ch, PC,
                                     quadtype, 0, return_channel_type=True)
                    mask = False
                # save our data, update max_chan for when we change quads
                spws[layer].append(spw)
                kinds[layer].append(kind)
                masks[layer].append(mask)
                if spw is not None:
                    if spw + prev_max[kind] > max_chan[kind]:
                        max_chan[kind] = spw + prev_max[kind]

            # then plot
            label = f'Layer {layer}'
            color = cmap[layer]
            if pintype == 'S':  # strips
                nums = (ma.masked_array(spws[layer], mask=masks[layer])
                        + prev_max['strip'])
                ax1.plot(nums, amps, color=color, label=label)
                for num, amp in zip(nums.data, amps):
                    strip_data[(num, layer)] = amp
            else:  # pads/wires
                pad_spws = []
                pad_amps = []
                pad_masks = []
                wire_spws = []
                wire_amps = []
                wire_masks = []
                # sort into pad and wire lists
                for s, k, a, m in zip(spws[layer], kinds[layer],
                                      amps, masks[layer]):
                    if k == "pad":
                        pad_spws.append(s)
                        pad_amps.append(a)
                        pad_masks.append(m)
                    elif k == "wire":
                        wire_spws.append(s)
                        wire_amps.append(a)
                        wire_masks.append(m)
                    else:
                        raise ValueError('why do you have strips?')
                pad_nums = (ma.masked_array(pad_spws, mask=pad_masks) +
                            prev_max['pad'])
                wire_nums = (ma.masked_array(wire_spws, mask=wire_masks) +
                             prev_max['wire'])
                ax2.plot(pad_nums, pad_amps, color=color, label=label)
                for num, amp in zip(pad_nums.data, pad_amps):
                    pad_data[(num, layer)] = amp
                ax3.plot(wire_nums, wire_amps, color=color, label=label)
                for num, amp in zip(wire_nums.data, wire_amps):
                    wire_data[(num, layer)] = amp
    

    # plot pad / wire data
    plot(HV0_p1, quadtype1, False, 'P')
    plot(HV1_p1, quadtype1, False, 'P')
    plot(Q2_pads, quadtype2, True, 'P')
    plot(Q3_pads, quadtype3, True, 'P')
    # plot strip data
    plot(HV0_sp1, quadtype1, False, 'S')
    plot(HV0_sp2, quadtype1, False, 'S')
    plot(HV1_sp1, quadtype1, False, 'S')
    plot(HV1_sp2, quadtype1, False, 'S')
    plot(Q2_sp1, quadtype2, True, 'S')
    plot(Q2_sp2, quadtype2, False, 'S')
    plot(Q3_sp1, quadtype3, True, 'S')
    plot(Q3_sp2, quadtype3, False, 'S')

    # plot legends
    l1Patch = mpatches.Patch(color=cmap[1], label='Layer 1')
    l2Patch = mpatches.Patch(color=cmap[2], label='Layer 2')
    l3Patch = mpatches.Patch(color=cmap[3], label='Layer 3')
    l4Patch = mpatches.Patch(color=cmap[4], label='Layer 4')

    ax1.legend(handles=[l1Patch, l2Patch, l3Patch, l4Patch], loc='best')
    ax1.set_ylabel('Strips: Amplitude (V)', fontsize=14)
    ax1.grid(which='major')

    ax2.set_ylabel('Pads: Amplitude (V)', fontsize=14)
    ax2.grid(which='major')

    ax3.set_xlabel('Strip/Pad/Wire Number', fontsize=14)
    ax3.set_ylabel('Wires: Amplitude (V)', fontsize=14)
    ax3.grid(which='major')

    if not os.path.exists('plots'):
        os.mkdir('plots')
    if save:
        plt.tight_layout()
        plt.savefig(f'plots/Wedge{wedgeNum}.png', dpi=300)
        pickle.dump(strip_data, open(f"pickles/strip_data_wedge_{wedgeNum}.p", "wb"))
        pickle.dump(pad_data, open(f"pickles/pad_data_wedge_{wedgeNum}.p", "wb"))
        pickle.dump(wire_data, open(f"pickles/wire_data_wedge_{wedgeNum}.p", "wb"))
        ax1.clear()
        ax2.clear()
        ax3.clear()
    else:
        plt.show()


if __name__ == "__main__":
    for i in range(1,71):
        print(i)
        full_wedge_plot(i, save=True)
