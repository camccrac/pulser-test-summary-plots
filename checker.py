import os

for folder in os.listdir('data'):
    if 'QS1' in folder and 'CONFIRM' in folder and 'Layer2' in folder and 'P_GFZP1' in folder:
        path = os.path.realpath('data/'+folder+'/database.csv')
        with open(path, 'r') as f:
            text = f.read()
            if '217, ,PASS' in text or '217, ,FAIL' in text:
                print(path)
